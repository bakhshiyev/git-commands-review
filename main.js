let section = document.querySelector('.section');
let heading = document.querySelector('h1');
let paragraph = document.querySelector('p');

// console.log(heading);
// console.log(section);
// console.log(paragraph);

section.addEventListener('click', handleSectionClick);
heading.addEventListener('click', handleHeadingClick);
paragraph.addEventListener('click', handleParagraphClick);

function handleSectionClick(e) {
    console.log('you clicked section');
}

function handleHeadingClick(e) {
    console.log('you clicked heading');
}

function handleParagraphClick(e) {
    console.log('you clicked paragraph');
}